/*
 * ConfigHandler.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.spider.common;

import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import net.minecraftforge.common.ForgeConfigSpec;
import org.apache.commons.lang3.tuple.Pair;

import java.nio.file.Paths;

public class ConfigHandler {

    public static void loadConfig() {
        CommentedFileConfig.builder(Paths.get("config", SpiderMod.SPIDERMOD_NAME, SpiderMod.MODID + ".toml")).build();
    }

    public static class CommonConfig {
        public static ForgeConfigSpec.IntValue tarantulaSpawnProb;
        public static ForgeConfigSpec.IntValue jumperSpawnProb;
        public static ForgeConfigSpec.IntValue widowSpawnProb;
        public static ForgeConfigSpec.IntValue huntsmanSpawnProb;
        public static ForgeConfigSpec.IntValue greenLynxSpawnProb;
        public static ForgeConfigSpec.IntValue minSpawn;
        public static ForgeConfigSpec.IntValue maxSpawn;
        public static ForgeConfigSpec.BooleanValue randomScale;

        public CommonConfig(ForgeConfigSpec.Builder builder) {
            builder.comment("SpiderMod Config")
                   .push("CommonConfig");

            minSpawn = builder
                    .comment("Minimum number of tarantulas to spawn at one time")
                    .translation("config.spidermod.minSpawn")
                    .defineInRange("minSpawn", 1, 1, 5);

            maxSpawn = builder
                    .comment("Maximum number of tarantulas to spawn at one time")
                    .translation("config.spidermod.maxSpawn")
                    .defineInRange("maxSpawn", 4, 1, 10);

            tarantulaSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.spidermod.tarantulaSpawnProb")
                    .defineInRange("tarantulaSpawnProb", 12, 0, 100);

            jumperSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.spidermod.jumperSpawnProb")
                    .defineInRange("jumperSpawnProb", 12, 0, 100);

            widowSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.spidermod.widowSpawnProb")
                    .defineInRange("widowSpawnProb", 8, 0, 100);

            huntsmanSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.spidermod.huntsmanSpawnProb")
                    .defineInRange("huntsmanSpawnProb", 8, 0, 100);

            greenLynxSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.spidermod.greenLynxSpawnProb")
                    .defineInRange("greenLynxSpawnProb", 8, 0, 100);

            randomScale = builder
                    .comment("Set to false to disable random scaling of tarantulas, default is true.")
                    .translation("config.spidermod.randomScale")
                    .define("randomScale", true);

            builder.pop();
        }

        public static boolean useRandomScaling() {
            return randomScale.get();
        }

        public static int getTarantulaSpawnProb() {
            return tarantulaSpawnProb.get();
        }

        public static int getJumperSpawnProb() { return jumperSpawnProb.get(); }

        public static int getWidowSpawnProb() { return widowSpawnProb.get(); }

        public static int getHUntsmanSpawnProb() { return huntsmanSpawnProb.get(); }

        public static int getGreenLynxSpawnProb() { return greenLynxSpawnProb.get(); }

        public static int getMinSpawn() {
            return minSpawn.get();
        }

        public static int getMaxSpawn() {
            return maxSpawn.get();
        }

    }

    static final ForgeConfigSpec commonSpec;
    public static final CommonConfig COMMON_CONFIG;
    static {
        final Pair<CommonConfig, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(CommonConfig::new);
        commonSpec = specPair.getRight();
        COMMON_CONFIG = specPair.getLeft();
    }
}
