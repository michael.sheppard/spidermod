/*
 * SpiderNearestAttackableTargetGoal.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.spider.common;

import net.minecraft.entity.EntityPredicate;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.ai.goal.TargetGoal;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.math.AxisAlignedBB;

import javax.annotation.Nullable;
import java.util.EnumSet;
import java.util.function.Predicate;

@SuppressWarnings("unused")
public class SpiderNearestAttackableTargetGoal<T extends LivingEntity> extends TargetGoal {
    protected final Class<T> targetClass;
    protected final int targetChance;
    protected LivingEntity nearestTarget;
    protected EntityPredicate targetEntitySelector;
    protected double targetDistance = 2.0;

    public SpiderNearestAttackableTargetGoal(MobEntity goalOwner, Class<T> targetClass, boolean checkSight) {
        this(goalOwner, targetClass, checkSight, false);
    }

    public SpiderNearestAttackableTargetGoal(MobEntity goalOwner, Class<T> targetClass, boolean checkSight, boolean nearbyOnly) {
        this(goalOwner, targetClass, 10, checkSight, nearbyOnly, null);
    }

    public SpiderNearestAttackableTargetGoal(MobEntity goalOwner, Class<T> targetClass, int targetChance, boolean checkSight, boolean nearbyOnly, @Nullable Predicate<LivingEntity> targetPredicate) {
        super(goalOwner, checkSight, nearbyOnly);
        this.targetClass = targetClass;
        this.targetChance = targetChance;
        setMutexFlags(EnumSet.of(Goal.Flag.TARGET));
        targetEntitySelector = (new EntityPredicate()).setDistance(getTargetDistance()).setCustomPredicate(targetPredicate);
    }

    public void setTargetDistance(double dist) {
        targetDistance = dist;
    }

    public double getTargetDistance() {
        return targetDistance;
    }

    public boolean shouldExecute() {
        if (targetChance > 0 && goalOwner.getRNG().nextInt(targetChance) != 0) {
            return false;
        } else {
            findNearestTarget();
            return nearestTarget != null;
        }
    }

    protected AxisAlignedBB getTargetableArea(double targetDistance) {
        return goalOwner.getBoundingBox().grow(targetDistance, targetDistance, targetDistance);
    }

    protected void findNearestTarget() {
        if (targetClass != PlayerEntity.class && targetClass != ServerPlayerEntity.class) {
            nearestTarget = goalOwner.world.func_225318_b(targetClass, targetEntitySelector, goalOwner, goalOwner.getPosX(), goalOwner.getPosYEye(), goalOwner.getPosZ(), getTargetableArea(targetDistance));
        } else {
            nearestTarget = goalOwner.world.getClosestPlayer(targetEntitySelector, goalOwner, goalOwner.getPosX(), goalOwner.getPosYEye(), goalOwner.getPosZ());
        }

    }

    public void startExecuting() {
        goalOwner.setAttackTarget(nearestTarget);
        super.startExecuting();
    }
}
