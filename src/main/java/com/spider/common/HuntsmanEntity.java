/*
 * HuntsmanEntity.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.spider.common;

import net.minecraft.block.BlockState;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.passive.ChickenEntity;
import net.minecraft.entity.passive.ParrotEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ArrowEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class HuntsmanEntity extends CreatureEntity {
    private static final DataParameter<Float> HEALTH = EntityDataManager.createKey(HuntsmanEntity.class, DataSerializers.FLOAT);
    private static final float MAX_SIZE = 0.30f;
    private static final float MIN_SIZE = 0.25f;

    private final float WIDTH = 3.0f;
    private final float HEIGHT = 1.0f;
    private final float scaleFactor;

    public EntitySize tarantulaSize = new EntitySize(WIDTH, HEIGHT, false);

    protected HuntsmanEntity(EntityType<? extends HuntsmanEntity> entity, World world) {
        super(entity, world);

        if (ConfigHandler.CommonConfig.useRandomScaling()) {
            float scale = rand.nextFloat();
            scaleFactor = Math.max(MIN_SIZE, Math.min(scale, MAX_SIZE));
        } else {
            scaleFactor = 0.1F;
        }
        setHealth(16);
        tarantulaSize.scale(WIDTH * scaleFactor, HEIGHT * scaleFactor);
    }

    @SuppressWarnings("unused")
    public HuntsmanEntity(World world) {
        this(SpiderMod.RegistryEvents.HUNTSMAN, world);
    }

    public float getScaleFactor() {
        return scaleFactor;
    }

    @Override
    protected void registerGoals() {
        goalSelector.addGoal(1, new SwimGoal(this));
        goalSelector.addGoal(3, new LeapAtTargetGoal(this, 0.6F));
        goalSelector.addGoal(4, new MeleeAttackGoal(this, 1.0D, true));
        goalSelector.addGoal(5, new WaterAvoidingRandomWalkingGoal(this, 0.8D));
        goalSelector.addGoal(6, new LookAtGoal(this, PlayerEntity.class, 8.0F));
        goalSelector.addGoal(6, new LookRandomlyGoal(this));
        targetSelector.addGoal(1, new HurtByTargetGoal(this));
        targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(this, ChickenEntity.class, false, true));
        targetSelector.addGoal(3, new NearestAttackableTargetGoal<>(this, ParrotEntity.class, false, true));
    }

    @Override
    protected void registerAttributes() {
        super.registerAttributes();
        getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(16.0);
        getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.3F);
    }

    @Override
    public boolean attackEntityAsMob(@Nonnull Entity entity) {
        if (entity.attackEntityFrom(DamageSource.causeMobDamage(this), 4)) {
            ((LivingEntity) entity).addPotionEffect(new EffectInstance(Effects.POISON, 140, 0));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void setAttackTarget(@Nullable LivingEntity livingEntity) {
        super.setAttackTarget(livingEntity);
    }

    @Override
    public boolean canDespawn(double distanceToPlayer) {
        return false;
    }

    @Nonnull
    @Override
    public EntityType<?> getType() {
        return SpiderMod.RegistryEvents.HUNTSMAN;
    }

    @Nonnull
    @Override
    public EntitySize getSize(@Nonnull Pose p) {
        return new EntitySize(WIDTH * scaleFactor, HEIGHT * scaleFactor, false);
    }

    @Override
    protected void registerData() {
        super.registerData();
        dataManager.register(HEALTH, getHealth());
    }

    @Override
    public void writeAdditional(@Nonnull CompoundNBT compound) {
        super.writeAdditional(compound);
    }

    @Override
    public void readAdditional(@Nonnull CompoundNBT compound) {
        super.readAdditional(compound);
    }

    @Override
    protected void updateAITasks() {
        dataManager.set(HEALTH, getHealth());
    }

    @Nonnull
    @Override
    public ResourceLocation getLootTable() {
        return new ResourceLocation(SpiderMod.MODID, SpiderMod.HUNTSMAN_LOOT);
    }

    @Override
    protected int getExperiencePoints(@Nonnull PlayerEntity playerEntity) {
        return 1 + world.rand.nextInt(4);
    }

    @Override
    protected SoundEvent getAmbientSound() {
        return SoundEvents.ENTITY_SPIDER_AMBIENT;
    }

    @Override
    protected SoundEvent getHurtSound(@Nonnull DamageSource damageSourceIn) {
        return SoundEvents.ENTITY_SPIDER_HURT;
    }

    @Override
    protected SoundEvent getDeathSound() {
        return SoundEvents.ENTITY_SPIDER_DEATH;
    }

    @Override
    protected void playStepSound(@Nonnull BlockPos pos, @Nonnull BlockState blockIn) {
        playSound(SoundEvents.ENTITY_SPIDER_STEP, 0.15F, 1.0F);
    }

    @Override
    protected float getSoundVolume() {
        return 0.25F;
    }

    @Override
    public void livingTick() {
        super.livingTick();
    }

    @Override
    public int getMaxSpawnedInChunk() {
        return 8;
    }

    @Override
    public boolean attackEntityFrom(@Nonnull DamageSource source, float amount) {
        if (isInvulnerable()) {
            return false;
        } else {
            Entity entity = source.getTrueSource();

            if (entity != null && !(entity instanceof PlayerEntity) && !(entity instanceof ArrowEntity)) {
                amount = (amount + 1.0F) / 2.0F;
            }

            return super.attackEntityFrom(source, amount);
        }
    }
}
