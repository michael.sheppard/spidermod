/*
 * TarantulaMod.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.spider.common;

import com.spider.client.*;
import net.minecraft.entity.*;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;

import java.util.*;

import net.minecraftforge.common.BiomeDictionary.Type;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistryEntry;
import net.minecraftforge.registries.ObjectHolder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@SuppressWarnings("unused")
@Mod(SpiderMod.MODID)
public class SpiderMod {

    public static final String MODID = "spidermod";
    public static final String SPIDERMOD_NAME = "spider";

    private static final Logger LOGGER = LogManager.getLogger(SpiderMod.MODID);

    // List of always excluded biome types
    private static final List<Type> excludedBiomeTypes = new ArrayList<>(Arrays.asList(
            Type.END,
            Type.NETHER,
            Type.VOID,
            Type.COLD,
            Type.OCEAN,
            Type.CONIFEROUS,
            Type.MOUNTAIN,
            Type.MUSHROOM,
            Type.SNOWY
    ));

    public static final String TARANTULA_NAME = "tarantula";
    public static final String TARANTULA_SPAWN_EGG_NAME = "tarantula_spawn_egg";

    public static final String JUMPER_NAME = "jumper";
    public static final String JUMPER_SPAWN_EGG_NAME = "jumper_spawn_egg";

    public static final String WIDOW_NAME = "widow";
    public static final String WIDOW_SPAWN_EGG_NAME = "widow_spawn_egg";

    public static final String HUNTSMAN_NAME = "huntsman";
    public static final String HUNTSMAN_SPAWN_EGG_NAME = "huntsman_spawn_egg";

    public static final String GREEN_LYNX_NAME = "green_lynx";
    public static final String GREEN_LYNX_SPAWN_EGG_NAME = "green_lynx_spawn_egg";

    public static final String TARANTULA_LOOT = "tarantula_loot";
    public static final String JUMPER_LOOT = "jumper_loot";
    public static final String WIDOW_LOOT = "widow_loot";
    public static final String HUNTSMAN_LOOT = "huntsman_loot";
    public static final String LYNX_LOOT = "green_lynx_loot";


    public SpiderMod() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::commonSetup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientSetup);

        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, ConfigHandler.commonSpec);

        MinecraftForge.EVENT_BUS.register(this);
    }

    public void commonSetup(final FMLCommonSetupEvent event) {
        ConfigHandler.loadConfig();

        registerSpawns();
        MinecraftForge.EVENT_BUS.register(new SpawnCheck());
    }

    public void clientSetup(final FMLClientSetupEvent event) {
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.TARANTULA, TarantulaRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.JUMPER, JumperRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.WIDOW, BlackWidowRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.HUNTSMAN, HuntsmanRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.GREEN_LYNX, GreenLynxRenderer::new);
    }

    @Mod.EventBusSubscriber(modid = SpiderMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
    @ObjectHolder(SpiderMod.MODID)
    public static class RegistryEvents {
        public static final EntityType<TarantulaEntity> TARANTULA = EntityType.Builder.<TarantulaEntity>create(TarantulaEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(80).build(MODID);

        public static final EntityType<JumperEntity> JUMPER = EntityType.Builder.<JumperEntity>create(JumperEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(80).build(MODID);

        public static final EntityType<BlackWidowEntity> WIDOW = EntityType.Builder.<BlackWidowEntity>create(BlackWidowEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(80).build(MODID);

        public static final EntityType<HuntsmanEntity> HUNTSMAN = EntityType.Builder.<HuntsmanEntity>create(HuntsmanEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(80).build(MODID);

        public static final EntityType<GreenLynxEntity> GREEN_LYNX = EntityType.Builder.<GreenLynxEntity>create(GreenLynxEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(80).build(MODID);

        @SubscribeEvent
        public static void onEntityRegistry(final RegistryEvent.Register<EntityType<?>> event) {
            event.getRegistry().registerAll(
                    setup(RegistryEvents.TARANTULA, SpiderMod.TARANTULA_NAME),
                    setup(RegistryEvents.JUMPER, SpiderMod.JUMPER_NAME),
                    setup(RegistryEvents.WIDOW, SpiderMod.WIDOW_NAME),
                    setup(RegistryEvents.HUNTSMAN, SpiderMod.HUNTSMAN_NAME),
                    setup(RegistryEvents.GREEN_LYNX, SpiderMod.GREEN_LYNX_NAME));
        }

        @SubscribeEvent
        public static void onItemRegistry(final RegistryEvent.Register<Item> event) {
            event.getRegistry().registerAll(
                    setup(new SpawnEggItem(RegistryEvents.TARANTULA, 0x8B2323, 0x0000CD, new Item.Properties().group(ItemGroup.MISC)), SpiderMod.TARANTULA_SPAWN_EGG_NAME),
                    setup(new SpawnEggItem(RegistryEvents.JUMPER, 0x483D8B, 0xDC143C, new Item.Properties().group(ItemGroup.MISC)), SpiderMod.JUMPER_SPAWN_EGG_NAME),
                    setup(new SpawnEggItem(RegistryEvents.WIDOW, 0x292929, 0xDC143C, new Item.Properties().group(ItemGroup.MISC)), SpiderMod.WIDOW_SPAWN_EGG_NAME),
                    setup(new SpawnEggItem(RegistryEvents.HUNTSMAN, 0x8B4513, 0xA52A2A, new Item.Properties().group(ItemGroup.MISC)), SpiderMod.HUNTSMAN_SPAWN_EGG_NAME),
                    setup(new SpawnEggItem(RegistryEvents.GREEN_LYNX, 0x32cc22, 0x29ff29, new Item.Properties().group(ItemGroup.MISC)), SpiderMod.GREEN_LYNX_SPAWN_EGG_NAME)
            );
        }

        public static <T extends IForgeRegistryEntry<T>> T setup(final T entry, final String name) {
            return setup(entry, new ResourceLocation(SpiderMod.MODID, name));
        }

        public static <T extends IForgeRegistryEntry<T>> T setup(final T entry, final ResourceLocation registryName) {
            entry.setRegistryName(registryName);
            return entry;
        }
    }

    private void registerSpawns() {
        Biome[] desertBiomes = getBiomes(Type.HOT, Type.DRY, Type.SANDY, Type.MESA);
        Biome[] woodsBiomes = getBiomes(Type.FOREST, Type.PLAINS, Type.SAVANNA, Type.JUNGLE);

        int minSpawn = ConfigHandler.CommonConfig.getMinSpawn();
        int maxSpawn = ConfigHandler.CommonConfig.getMaxSpawn();
        int tarantulaSpawnProb = ConfigHandler.CommonConfig.getTarantulaSpawnProb();
        int jumperSpawnProb = ConfigHandler.CommonConfig.getJumperSpawnProb();
        int widowSpawnProb = ConfigHandler.CommonConfig.getWidowSpawnProb();
        int huntsmanSpawnProb = ConfigHandler.CommonConfig.getHUntsmanSpawnProb();
        int greenLynxSpawnProb = ConfigHandler.CommonConfig.getGreenLynxSpawnProb();

        registerEntitySpawn(RegistryEvents.TARANTULA, desertBiomes, tarantulaSpawnProb, minSpawn, maxSpawn);
        registerEntitySpawn(RegistryEvents.JUMPER, woodsBiomes, jumperSpawnProb, minSpawn, maxSpawn);
        // minSpawn for maxSpawn on purpose, don't need too many of these running around
        registerEntitySpawn(RegistryEvents.WIDOW, woodsBiomes, widowSpawnProb, minSpawn, minSpawn);
        registerEntitySpawn(RegistryEvents.HUNTSMAN, woodsBiomes, huntsmanSpawnProb, minSpawn, maxSpawn);
        registerEntitySpawn(RegistryEvents.GREEN_LYNX, woodsBiomes, greenLynxSpawnProb, minSpawn, maxSpawn);
    }

    private static Biome[] getBiomes(Type... types) {
        LinkedList<Biome> list = new LinkedList<>();
        Collection<Biome> biomes = ForgeRegistries.BIOMES.getValues();

        for (Biome biome : biomes) {
            Set<Type> bTypes = BiomeDictionary.getTypes(biome);

            // we exclude certain biomes
            if (excludeThisBiome(bTypes)) {
                continue;
            }
            // process remaining biomes
            for (Type type : types) {
                if (BiomeDictionary.hasType(biome, type)) {
                    if (!list.contains(biome)) {
                        list.add(biome);
                        getLogger().info("Adding: " + biome.getRegistryName() + " biome for spawning");
                    }
                }
            }
        }
        return list.toArray(new Biome[0]);
    }

    private static boolean excludeThisBiome(Set<Type> types) {
        boolean excludeBiome = false;
        for (Type ex : excludedBiomeTypes) {
            if (types.contains(ex)) {
                excludeBiome = true;
                break;
            }
        }
        return excludeBiome;
    }

    private void registerEntitySpawn(EntityType<? extends LivingEntity> type, Biome[] biomes, int spawnProb, int minSpawn, int maxSpawn) {
        if (spawnProb <= 0) {
            // do not spawn this entity
            return;
        }

        for (Biome bgb : biomes) {
            Biome biome = ForgeRegistries.BIOMES.getValue(bgb.getRegistryName());
            if (biome != null) {
                biome.getSpawns(EntityClassification.CREATURE).add(new Biome.SpawnListEntry(type, spawnProb, minSpawn, maxSpawn));
            }
        }
    }

    public static Logger getLogger() {
        return LOGGER;
    }
}
