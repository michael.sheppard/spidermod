/*
 * SpawnCheck.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.spider.common;

import net.minecraft.entity.Entity;
import net.minecraftforge.event.entity.living.LivingSpawnEvent;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class SpawnCheck {

    @SuppressWarnings("unused")
    @SubscribeEvent
    public Event.Result onCheckSpawnEvent(LivingSpawnEvent.CheckSpawn event) {
        Event.Result result = Event.Result.DEFAULT;
        Entity entity = event.getEntity();

        if (entity instanceof TarantulaEntity) {
            result = Event.Result.ALLOW;
//            SpiderMod.getLogger().debug("Spawning TarantulaEntity");
        }

        if (entity instanceof JumperEntity) {
            result = Event.Result.ALLOW;
//            SpiderMod.getLogger().debug("Spawning JumperEntity");
        }

        if (entity instanceof BlackWidowEntity) {
            result = Event.Result.ALLOW;
        }
        
        if (entity instanceof GreenLynxEntity) {
            result = Event.Result.ALLOW;
        }
        
        if (entity instanceof HuntsmanEntity) {
            result = Event.Result.ALLOW;
        }

        event.setResult(result);
        return result;
    }
}
