/*
 * GreenLynxRenderer.java
 *  
 *  Copyright (c) 2020 Michael Sheppard
 *  
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.spider.client;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.spider.common.GreenLynxEntity;
import com.spider.common.SpiderMod;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nonnull;

public class GreenLynxRenderer<T extends GreenLynxEntity> extends MobRenderer<T, GreenLynxModel<T>> {
    private static final ResourceLocation LYNX_TEXTURE =  new ResourceLocation(SpiderMod.MODID, "textures/entity/green_lynx/green_lynx.png");

    public GreenLynxRenderer(EntityRendererManager renderManagerIn) {
        super(renderManagerIn, new GreenLynxModel<>(), 0.8f);
        addLayer(new GreenLynxEyesLayer<>(this));
        shadowSize *= 0.15F;
    }

    @Override
    protected void preRenderCallback(GreenLynxEntity jumperEntity, MatrixStack matrixStack, float partialTickTime) {
        float scale = jumperEntity.getScaleFactor();
        matrixStack.scale(scale, scale, scale);
    }

    @Nonnull
    public ResourceLocation getEntityTexture(@Nonnull GreenLynxEntity entity) {
        return LYNX_TEXTURE;
    }
}
