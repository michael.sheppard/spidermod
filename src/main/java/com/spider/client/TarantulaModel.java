/*
 * TarantulaModel.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.spider.client;

import com.google.common.collect.ImmutableList;
import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

import javax.annotation.Nonnull;

public class TarantulaModel<T extends Entity> extends SegmentedModel<T> {

    private final ModelRenderer head;
    private final ModelRenderer thorax;
    private final ModelRenderer body;
    private final ModelRenderer leg1;
    private final ModelRenderer leg2;
    private final ModelRenderer leg3;
    private final ModelRenderer leg4;
    private final ModelRenderer leg5;
    private final ModelRenderer leg6;
    private final ModelRenderer leg7;
    private final ModelRenderer leg8;

    public TarantulaModel() {
        head = new ModelRenderer(this, 32, 4);
        head.addBox(-4.0F, -4.0F, -8.0F, 8.0F, 8.0F, 8.0F, 0.0F);
        head.setRotationPoint(0.0F, 15.0F, -3.0F);

        thorax = new ModelRenderer(this, 0, 0);
        thorax.addBox(-3.0F, -3.0F, -3.0F, 6.0F, 6.0F, 6.0F, 0.0F);
        thorax.setRotationPoint(0.0F, 15.0F, 0.0F);

        body = new ModelRenderer(this, 0, 12);
        body.addBox(-5.0F, -4.0F, -6.0F, 10.0F, 8.0F, 12.0F, 0.0F);
        body.setRotationPoint(0.0F, 15.0F, 9.0F);

        leg1 = new ModelRenderer(this, 18, 0);
        leg1.addBox(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        leg1.setRotationPoint(-4.0F, 15.0F, 2.0F);

        leg2 = new ModelRenderer(this, 18, 0);
        leg2.addBox(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        leg2.setRotationPoint(4.0F, 15.0F, 2.0F);

        leg3 = new ModelRenderer(this, 18, 0);
        leg3.addBox(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        leg3.setRotationPoint(-4.0F, 15.0F, 1.0F);

        leg4 = new ModelRenderer(this, 18, 0);
        leg4.addBox(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        leg4.setRotationPoint(4.0F, 15.0F, 1.0F);

        leg5 = new ModelRenderer(this, 18, 0);
        leg5.addBox(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        leg5.setRotationPoint(-4.0F, 15.0F, 0.0F);

        leg6 = new ModelRenderer(this, 18, 0);
        leg6.addBox(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        leg6.setRotationPoint(4.0F, 15.0F, 0.0F);

        leg7 = new ModelRenderer(this, 18, 0);
        leg7.addBox(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        leg7.setRotationPoint(-4.0F, 15.0F, -1.0F);

        leg8 = new ModelRenderer(this, 18, 0);
        leg8.addBox(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        leg8.setRotationPoint(4.0F, 15.0F, -1.0F);
    }

    @Override
    @Nonnull
    public Iterable<ModelRenderer> getParts() {
        return ImmutableList.of(head, thorax, body, leg1, leg2, leg3, leg4, leg5, leg6, leg7, leg8);
    }

    @Override
    public void setRotationAngles(@Nonnull T entity, float x, float y, float z, float partialTicks, float scale) {
        head.rotateAngleY = partialTicks * ((float) Math.PI / 180F);
        head.rotateAngleX = scale * ((float) Math.PI / 180F);
        float legAngle0 = ((float) Math.PI / 4F);
        leg1.rotateAngleZ = (-legAngle0);
        leg2.rotateAngleZ = (legAngle0);
        leg3.rotateAngleZ = -0.58119464F;
        leg4.rotateAngleZ = 0.58119464F;
        leg5.rotateAngleZ = -0.58119464F;
        leg6.rotateAngleZ = 0.58119464F;
        leg7.rotateAngleZ = (-legAngle0);
        leg8.rotateAngleZ = (legAngle0);
        float legAngle = ((float) Math.PI / 8F);
        leg1.rotateAngleY = (legAngle0);
        leg2.rotateAngleY = (-legAngle0);
        leg3.rotateAngleY = (legAngle);
        leg4.rotateAngleY = (-legAngle);
        leg5.rotateAngleY = (-legAngle);
        leg6.rotateAngleY = (legAngle);
        leg7.rotateAngleY = (-legAngle0);
        leg8.rotateAngleY = (legAngle0);
        float f3 = -(MathHelper.cos(x * 0.6662F * 2.0F + 0.0F) * 0.4F) * y;
        float f4 = -(MathHelper.cos(x * 0.6662F * 2.0F + (float) Math.PI) * 0.4F) * y;
        float f5 = -(MathHelper.cos(x * 0.6662F * 2.0F + ((float) Math.PI / 2F)) * 0.4F) * y;
        float f6 = -(MathHelper.cos(x * 0.6662F * 2.0F + ((float) Math.PI * 1.5F)) * 0.4F) * y;
        float f7 = Math.abs(MathHelper.sin(x * 0.6662F + 0.0F) * 0.4F) * y;
        float f8 = Math.abs(MathHelper.sin(x * 0.6662F + (float) Math.PI) * 0.4F) * y;
        float f9 = Math.abs(MathHelper.sin(x * 0.6662F + ((float) Math.PI / 2F)) * 0.4F) * y;
        float f10 = Math.abs(MathHelper.sin(x * 0.6662F + ((float) Math.PI * 1.5F)) * 0.4F) * y;
        leg1.rotateAngleY += f3;
        leg2.rotateAngleY += -f3;
        leg3.rotateAngleY += f4;
        leg4.rotateAngleY += -f4;
        leg5.rotateAngleY += f5;
        leg6.rotateAngleY += -f5;
        leg7.rotateAngleY += f6;
        leg8.rotateAngleY += -f6;
        leg1.rotateAngleZ += f7;
        leg2.rotateAngleZ += -f7;
        leg3.rotateAngleZ += f8;
        leg4.rotateAngleZ += -f8;
        leg5.rotateAngleZ += f9;
        leg6.rotateAngleZ += -f9;
        leg7.rotateAngleZ += f10;
        leg8.rotateAngleZ += -f10;
    }

}
