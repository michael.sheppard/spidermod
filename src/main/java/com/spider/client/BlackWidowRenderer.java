/*
 * BlackWidowRenderer.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.spider.client;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.spider.common.BlackWidowEntity;
import com.spider.common.SpiderMod;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nonnull;

@OnlyIn(Dist.CLIENT)
public class BlackWidowRenderer<T extends BlackWidowEntity> extends MobRenderer<T, BlackWidowModel<T>> {
    private static final ResourceLocation WIDOW_TEXTURES =  new ResourceLocation(SpiderMod.MODID, "textures/entity/widow/widow.png");

    public BlackWidowRenderer(EntityRendererManager renderManagerIn) {
        super(renderManagerIn, new BlackWidowModel<>(), 0.8f);
        addLayer(new BlackWidowEyesLayer<>(this));
        shadowSize *= 0.15F;
    }

    @Override
    protected void preRenderCallback(BlackWidowEntity jumperEntity, MatrixStack matrixStack, float partialTickTime) {
        float scale = jumperEntity.getScaleFactor();
        matrixStack.scale(scale, scale, scale);
    }

    @Nonnull
    public ResourceLocation getEntityTexture(@Nonnull BlackWidowEntity entity) {
        return WIDOW_TEXTURES;
    }
}
